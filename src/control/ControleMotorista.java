package control;

import java.util.ArrayList;

import model.Motorista;


public class ControleMotorista {
	private ArrayList<Motorista> listaMotoristas;
	
	public ControleMotorista(){
		listaMotoristas = new ArrayList<Motorista>();
	}
	
	public void adicionarMotorista(Motorista umMotorista){
		listaMotoristas.add(umMotorista);
	}
	
	public void removerMotorista(Motorista umMotorista){
		listaMotoristas.remove(umMotorista);
	}
	
	public Motorista pesquisarMotorista(String nome){
		for(Motorista umMotorista: listaMotoristas){
			if(umMotorista.getNome().equalsIgnoreCase(nome))
				return umMotorista;
		}
		return null;
	}

}
