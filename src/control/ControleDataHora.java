package control;

import java.text.SimpleDateFormat;
import java.util.Date;


public class ControleDataHora {
    
    public String MostraData(){
       Date data = new Date();
       SimpleDateFormat dformatador = new SimpleDateFormat("dd/MM/yyyy");
       String sData = dformatador.format(data);
       return sData;
    }
    public String MostraHora(){
       Date data = new Date();
       SimpleDateFormat hformatador = new SimpleDateFormat("hh:mm");
       String sHora = hformatador.format(data);
       return sHora;
    }

}
