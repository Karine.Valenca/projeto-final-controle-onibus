package control;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import model.Funcionario;


public class ControleFuncionario {
	private static ArrayList<Funcionario> listaFuncionarios;
	
	public ControleFuncionario(){
		listaFuncionarios = new ArrayList<Funcionario>();
	}
	
	public void adicionarFuncionario(Funcionario umFuncionario){
		listaFuncionarios.add(umFuncionario);
	}
	
	public void removerFuncionario(Funcionario umFuncionario){
		listaFuncionarios.remove(umFuncionario);
	}
	
	public Funcionario pesquisarFuncionario(String nome){
		for(Funcionario umFuncionario: listaFuncionarios){
			if(umFuncionario.getNome().equalsIgnoreCase(nome))
				return umFuncionario;
		}
		return null;
	}
	
}
