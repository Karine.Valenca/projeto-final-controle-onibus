package control;

import java.util.ArrayList;

import model.Gerente;

public class ControleGerente {
	private ArrayList<Gerente> listaGerentes;
	
	public ControleGerente(){
		listaGerentes = new ArrayList<Gerente>();
	}
	
	public void adicionarGerente(Gerente umGerente){
		listaGerentes.add(umGerente);
	}
	
	public void removerGerente(Gerente umGerente){
		listaGerentes.remove(umGerente);
	}
	
	public Gerente pesquisar(String nome){
		for(Gerente umGerente: listaGerentes){
			if(umGerente.getNome().equalsIgnoreCase(nome))
				return umGerente;
		}
		return null;
	}
}
