package control;

import java.util.ArrayList;
import model.Onibus;

public class ControleOnibus {
    private ArrayList<Onibus> listaOnibus;
    
    public ControleOnibus(){
        listaOnibus = new ArrayList<Onibus>();
    }
    
    public void adicionarOnibus(Onibus umOnibus){
        listaOnibus.add(umOnibus);
    }
    
    public void removerOnibus(Onibus umOnibus){
        listaOnibus.remove(umOnibus);
    }
    
    public Onibus pesquisarOnibus(String placa){
		for(Onibus umOnibus: listaOnibus){
			if(umOnibus.getPlaca().equalsIgnoreCase(placa))
				return umOnibus;
		}
		return null;
	}
}
