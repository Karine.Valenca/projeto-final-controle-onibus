package control;

import java.util.ArrayList;

import model.Cobrador;


public class ControleCobrador {
	private ArrayList<Cobrador> listaCobradores;
	
	public ControleCobrador(){
		listaCobradores = new ArrayList<Cobrador>();
	}
	
	public void adicionarCobrador(Cobrador umCobrador){
		listaCobradores.add(umCobrador);
	}
	
	public void removerCobrador(Cobrador umCobrador){
		listaCobradores.remove(umCobrador);
	}
	
	public Cobrador pesquisarCobrador(String nome){
		for(Cobrador umCobrador: listaCobradores){
			if(umCobrador.getNome().equalsIgnoreCase(nome))
				return umCobrador;
		}
		return null;
	}

}

