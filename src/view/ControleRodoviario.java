package view;

import control.ControleCobrador;
import control.ControleDataHora;
import control.ControleMotorista;
import control.ControleFuncionario;
import control.ControleOnibus;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import model.Cobrador;
import model.Funcionario;
import model.Motorista;
import model.Onibus;

public class ControleRodoviario extends javax.swing.JFrame {
    Funcionario umFuncionario = new Funcionario();
    ControleFuncionario umControleFuncionario = new ControleFuncionario();
    Motorista umMotorista = new Motorista();
    ControleMotorista umControleMotorista = new ControleMotorista();
    Cobrador umCobrador = new Cobrador();
    ControleCobrador umControleCobrador = new ControleCobrador();
    Onibus umOnibus = new Onibus();
    ControleOnibus umControleOnibus = new ControleOnibus();
    ControleDataHora umControleDataHora = new ControleDataHora();
    
    
    DefaultTableModel model;
    DefaultTableModel modelOnibus;
    
    private boolean alteracao;
    private final byte MOTORISTA = 1;
    private final byte COBRADOR = 2;
   
    public ControleRodoviario() {
        initComponents();
        model = (DefaultTableModel) jTableListaFuncionarios.getModel();
        modelOnibus = (DefaultTableModel) jTableListaOnibus.getModel();
        this.alteracao = false;
        this.habilitarCampos();
        this.jTextFieldHorarioSaida.setText(umControleDataHora.MostraHora());
        this.jTextFieldData.setText(umControleDataHora.MostraData());
        limparTela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabelTituloFuncionario = new javax.swing.JLabel();
        jLabelNome = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabelNumeroIdenficacao = new javax.swing.JLabel();
        jTextFieldNumeroIdentificacao = new javax.swing.JTextField();
        jLabelTipo = new javax.swing.JLabel();
        jComboBoxTipo = new javax.swing.JComboBox();
        jButtonSalvarFuncionario = new javax.swing.JButton();
        jButtonNovo = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListaFuncionarios = new javax.swing.JTable();
        jButtonPesquisar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabelTituloOnibus = new javax.swing.JLabel();
        jLabelNomeDaLinha = new javax.swing.JLabel();
        jTextFieldNomeDaLinha = new javax.swing.JTextField();
        jLabelNumeroDaLinha = new javax.swing.JLabel();
        jTextFieldNumeroDaLinha = new javax.swing.JTextField();
        jLabelPlaca = new javax.swing.JLabel();
        jTextFieldPlaca = new javax.swing.JTextField();
        jLabelHorarioSaida = new javax.swing.JLabel();
        jTextFieldHorarioSaida = new javax.swing.JTextField();
        jLabelMotoristaOnibus = new javax.swing.JLabel();
        jTextFieldMotoristaOnibus = new javax.swing.JTextField();
        jLabelCobradorOnibus = new javax.swing.JLabel();
        jTextFieldCobradorOnibus = new javax.swing.JTextField();
        jButtonSalvarOnibus = new javax.swing.JButton();
        jButtonCancelarOnibus = new javax.swing.JButton();
        jLabelData = new javax.swing.JLabel();
        jTextFieldData = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableListaOnibus = new javax.swing.JTable();
        jButtonExcluirOnibus = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabelTituloFuncionario.setText("Informe os dados do funcionário");

        jLabelNome.setText("Nome:");

        jLabelNumeroIdenficacao.setText("Nº Identificação:");

        jLabelTipo.setText("Função:");

        jComboBoxTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "Motorista", "Cobrador" }));

        jButtonSalvarFuncionario.setText("Salvar");
        jButtonSalvarFuncionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarFuncionarioActionPerformed(evt);
            }
        });

        jButtonNovo.setText("Novo");
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(143, 143, 143)
                        .addComponent(jLabelTituloFuncionario))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNumeroIdenficacao)
                            .addComponent(jLabelNome)
                            .addComponent(jLabelTipo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jComboBoxTipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextFieldNumeroIdentificacao)
                            .addComponent(jTextFieldNome, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jButtonSalvarFuncionario)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonNovo)))))
                .addContainerGap(200, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTituloFuncionario)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNome)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNumeroIdenficacao)
                    .addComponent(jTextFieldNumeroIdentificacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTipo)
                    .addComponent(jComboBoxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSalvarFuncionario)
                    .addComponent(jButtonNovo))
                .addContainerGap(171, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro Funcionário", jPanel2);

        jTableListaFuncionarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Número Idenficação", "Função"
            }
        ));
        jTableListaFuncionarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableListaFuncionariosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableListaFuncionarios);

        jButtonPesquisar.setText("Pesquisar");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jButtonPesquisar)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonExcluir)))
                .addContainerGap(101, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonPesquisar)
                    .addComponent(jButtonExcluir))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Lista Funcionários", jPanel3);

        jLabelTituloOnibus.setText("Informe os dados do ônibus");

        jLabelNomeDaLinha.setText("Nome da linha:");

        jLabelNumeroDaLinha.setText("Nº da linha:");

        jLabelPlaca.setText("Placa:");

        jLabelHorarioSaida.setText("Horário saída:");

        jLabelMotoristaOnibus.setText("Motorista:");

        jLabelCobradorOnibus.setText("Cobrador:");

        jButtonSalvarOnibus.setText("Salvar");
        jButtonSalvarOnibus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarOnibusActionPerformed(evt);
            }
        });

        jButtonCancelarOnibus.setText("Cancelar");
        jButtonCancelarOnibus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarOnibusActionPerformed(evt);
            }
        });

        jLabelData.setText("Data:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelPlaca)
                            .addComponent(jLabelNomeDaLinha)
                            .addComponent(jLabelNumeroDaLinha)
                            .addComponent(jLabelData)
                            .addComponent(jLabelHorarioSaida)
                            .addComponent(jLabelMotoristaOnibus)
                            .addComponent(jLabelCobradorOnibus))
                        .addGap(29, 29, 29)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButtonSalvarOnibus)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonCancelarOnibus))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTextFieldCobradorOnibus, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldMotoristaOnibus, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldHorarioSaida, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldData, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldNomeDaLinha, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                .addComponent(jTextFieldNumeroDaLinha, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldPlaca, javax.swing.GroupLayout.Alignment.LEADING))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(205, 205, 205)
                        .addComponent(jLabelTituloOnibus)))
                .addContainerGap(171, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTituloOnibus)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNomeDaLinha)
                    .addComponent(jTextFieldNomeDaLinha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNumeroDaLinha)
                    .addComponent(jTextFieldNumeroDaLinha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPlaca)
                    .addComponent(jTextFieldPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelData)
                    .addComponent(jTextFieldData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelHorarioSaida)
                    .addComponent(jTextFieldHorarioSaida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelMotoristaOnibus)
                    .addComponent(jTextFieldMotoristaOnibus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSalvarOnibus)
                            .addComponent(jButtonCancelarOnibus)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCobradorOnibus)
                            .addComponent(jTextFieldCobradorOnibus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Entrada/Saída de Ônibus", jPanel1);

        jTableListaOnibus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nº da Linha", "Placa", "Data", "Horário Saída"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableListaOnibus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableListaOnibusMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableListaOnibus);

        jButtonExcluirOnibus.setText("Excluir");
        jButtonExcluirOnibus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirOnibusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonExcluirOnibus))
                .addContainerGap(101, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonExcluirOnibus)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Lista Ônibus", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 573, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 407, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jButtonSalvarFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarFuncionarioActionPerformed
    if(this.validarCampos() == false){
        return;
    }else{
        switch(jComboBoxTipo.getSelectedIndex()){
            case 1:
                umMotorista = this.salvarMotorista();
                umFuncionario = this.salvarFuncionario();
                umFuncionario.tipo = "Motorista";
            
            case 2:
                umCobrador = this.salvarCobrador();
                umFuncionario = this.salvarFuncionario();
                umFuncionario.tipo = "Cobrador";
        }
    }
    
    this.aviso("Funcionário adicionado com sucesso!");
    
    model.insertRow(model.getRowCount(), new Object[]{jTextFieldNome.getText(),
        jTextFieldNumeroIdentificacao.getText(), jComboBoxTipo.getSelectedItem()});
    limparTela();
    this.alteracao = false;
    this.habilitarCampos();
}//GEN-LAST:event_jButtonSalvarFuncionarioActionPerformed

private void jTableListaFuncionariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaFuncionariosMouseClicked
        
}//GEN-LAST:event_jTableListaFuncionariosMouseClicked

private void jButtonSalvarOnibusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarOnibusActionPerformed
    if(this.validarCamposOnibus()==false){
        return;
    }else{
        umOnibus = this.salvarOnibus();
        this.aviso("Os dados do ônibus foram salvos com sucesso!");
         modelOnibus.insertRow(modelOnibus.getRowCount(), new Object[]{jTextFieldNumeroDaLinha.getText(),
        jTextFieldPlaca.getText(), jTextFieldData.getText(), jTextFieldHorarioSaida.getText()});
        limparTela();
    }
    
    
}//GEN-LAST:event_jButtonSalvarOnibusActionPerformed

private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
    String pesquisa = JOptionPane.showInputDialog("Digite o nome do funcionário:");
    if(pesquisa != null){
        pesquisar(pesquisa);
    }
}//GEN-LAST:event_jButtonPesquisarActionPerformed

private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed
    this.alteracao = true;
    this.habilitarCampos();
    this.limparTela();
}//GEN-LAST:event_jButtonNovoActionPerformed

private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
    
    if (jTableListaFuncionarios.getSelectedRow() >= 0){ 
        if(umFuncionario.tipo == "Motorista"){
        umControleFuncionario.removerFuncionario(umFuncionario);
        umControleMotorista.removerMotorista(umMotorista);
        
    }else if(umFuncionario.tipo == "Cobrador"){
        umControleFuncionario.removerFuncionario(umFuncionario);
        //umControleCobrador.removerCobrador(umCobrador);
    }
        model.removeRow(jTableListaFuncionarios.getSelectedRow());  
        jTableListaFuncionarios.setModel(model);
        this.aviso("Funcionario removido com sucesso");
        limparTela();
    }else{
        this.aviso("Selecione um funcionário");
        }
    
}//GEN-LAST:event_jButtonExcluirActionPerformed

private void jButtonExcluirOnibusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirOnibusActionPerformed
    if(jTableListaOnibus.getSelectedRow() >= 0){
        umControleOnibus.removerOnibus(umOnibus);
        modelOnibus.removeRow(jTableListaOnibus.getSelectedRow());
        this.aviso("Ônibus removido com sucesso");
        
    }else
        this.aviso("Selecione um ônibus");
    limparTela();
}//GEN-LAST:event_jButtonExcluirOnibusActionPerformed

private void jTableListaOnibusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaOnibusMouseClicked
    if (evt.getClickCount() > 1) {  
  // faça algo  
    DefaultTableModel modelOnibus = (DefaultTableModel) jTableListaOnibus.getModel();
    String placa = (String) modelOnibus.getValueAt(jTableListaOnibus.getSelectedRow(), 1);
    this.pesquisarOnibus(placa);
    }
}//GEN-LAST:event_jTableListaOnibusMouseClicked

private void jButtonCancelarOnibusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarOnibusActionPerformed
    limparTela();
}//GEN-LAST:event_jButtonCancelarOnibusActionPerformed

    public void pesquisar(String nome){
        Funcionario funcionarioPesquisado = umControleFuncionario.pesquisarFuncionario(nome);
        if(funcionarioPesquisado == null ){
            this.aviso("Funcionário não registrado");
            }else if(funcionarioPesquisado != null){
                this.jTabbedPane1.setSelectedIndex(0);
                this.umFuncionario = funcionarioPesquisado;
                this.jTextFieldNome.setText(umFuncionario.getNome());
                this.jTextFieldNumeroIdentificacao.setText(umFuncionario.getNumeroIdentificacao());
                if(umFuncionario.tipo == "Motorista"){
                    this.jComboBoxTipo.setSelectedIndex(1);
                }else if(umFuncionario.tipo == "Cobrador"){
                    this.jComboBoxTipo.setSelectedIndex(2);
                }
            }
    }
    
    public void pesquisarOnibus(String placa){
        Onibus onibusPesquisado = umControleOnibus.pesquisarOnibus(placa);
        if(onibusPesquisado == null){
            this.aviso("Onibus não encontrado");
        }else if(onibusPesquisado != null){
            this.jTabbedPane1.setSelectedIndex(2);
            this.umOnibus = onibusPesquisado;
            this.jTextFieldNumeroDaLinha.setText(umOnibus.getNumeroDaLinha());
            this.jTextFieldNomeDaLinha.setText(umOnibus.getNomeDaLinha());
            this.jTextFieldPlaca.setText(umOnibus.getPlaca());
            this.jTextFieldData.setText(umOnibus.getData());
            this.jTextFieldHorarioSaida.setText(umOnibus.getHorarioSaida());
           // this.jTextFieldMotoristaOnibus.setText(umOnibus.getMotorista().getNome());
            //this.jTextFieldCobradorOnibus.setText(umOnibus.getMotorista().getNome());
        }
        
    }
    
    public void habilitarCampos(){
        this.jTextFieldNome.setEnabled(alteracao);
        this.jTextFieldNumeroIdentificacao.setEnabled(alteracao);
        this.jComboBoxTipo.setEnabled(alteracao);
        this.jButtonSalvarFuncionario.setEnabled(alteracao);
        //this.jButtonExcluir.setEnabled(alteracao);
    }
    
    public void limparTela(){
        SimpleDateFormat hora = new SimpleDateFormat("HH:mm:aa");  
        this.jTextFieldNome.setText("");
        this.jTextFieldNumeroIdentificacao.setText("");
        this.jComboBoxTipo.setSelectedIndex(0);
        this.jTextFieldNumeroDaLinha.setText("");
        this.jTextFieldNomeDaLinha.setText("");
        this.jTextFieldPlaca.setText("");
        this.jTextFieldHorarioSaida.setText(umControleDataHora.MostraHora());
        this.jTextFieldMotoristaOnibus.setText("");
        this.jTextFieldCobradorOnibus.setText("");
    }
    
    public Motorista salvarMotorista(){
        String nome = this.jTextFieldNome.getText();
        String numeroIdentificacao = this.jTextFieldNumeroIdentificacao.getText();
        
        
        umMotorista = new Motorista(nome, numeroIdentificacao);
        
        umControleMotorista.adicionarMotorista(umMotorista);
        
        return umMotorista;
    }   
    
    public Cobrador salvarCobrador(){
        String nome = this.jTextFieldNome.getText();
        String numeroIdentificacao = this.jTextFieldNumeroIdentificacao.getText();
        
        
        
        umCobrador = new Cobrador(nome, numeroIdentificacao);
        
        umControleCobrador.adicionarCobrador(umCobrador);
        
        return umCobrador;
    }
    
    public Funcionario salvarFuncionario(){
        String nome = this.jTextFieldNome.getText();
        String numeroIdentificacao = this.jTextFieldNumeroIdentificacao.getText();
        
        
        umFuncionario = new Funcionario(nome, numeroIdentificacao);
        
        umControleFuncionario.adicionarFuncionario(umFuncionario);
        return umFuncionario;        
    }
    
    public Onibus salvarOnibus(){
        String numeroDaLinha = this.jTextFieldNumeroDaLinha.getText();
        String nomeDaLinha = this.jTextFieldNomeDaLinha.getText();
        String placa = this.jTextFieldPlaca.getText();
        String data = this.jTextFieldData.getText();
        String horarioSaida = this.jTextFieldHorarioSaida.getText();
        String motorista = this.jTextFieldMotoristaOnibus.getText();
        String cobrador = this.jTextFieldCobradorOnibus.getText();
        
        umOnibus = new Onibus(numeroDaLinha, nomeDaLinha, placa, data, horarioSaida);
        
        umControleOnibus.adicionarOnibus(umOnibus);
        return umOnibus;
    }
    
    private void aviso(String informacao){
                JOptionPane.showMessageDialog(this, informacao, "Atenção", 
                JOptionPane.INFORMATION_MESSAGE);
    }
    
    
    private boolean validarCampos(){
        String tipo = this.jComboBoxTipo.getSelectedItem().toString();
        if(this.jTextFieldNome.getText().trim().length() == 0){
            this.aviso("O campo nome não foi informado.");
            this.jTextFieldNome.requestFocus();
            return false;
        }   else if(this.jTextFieldNumeroIdentificacao.getText().trim().length() == 0){
            this.aviso("O campo Nº informação não foi informado.");
            this.jTextFieldNumeroIdentificacao.requestFocus();
            return false;
        }   else if(tipo.trim().length() == 0){
            this.aviso("O campo função não foi informado.");
            this.jComboBoxTipo.requestFocus();
            return false;
        }        
        else return true;
    }
 
    private boolean validarCamposOnibus(){
        if(this.jTextFieldNomeDaLinha.getText().trim().length() == 0){
                this.aviso("O campo Nome da linha não foi informado");
                return false;
        }   else if(this.jTextFieldNumeroDaLinha.getText().trim().length() == 0){
                this.aviso("O campo Nº da linha não foi informado");
                return false;
        }   else if(this.jTextFieldPlaca.getText().trim().length() == 0){
                this.aviso("O campo Placa não foi informado");
                return false;
        }   else if(this.jTextFieldMotoristaOnibus.getText().trim().length() == 0){
                this.aviso("O campo Motorista não foi informado");
                return false;
        }   else if(this.jTextFieldCobradorOnibus.getText().trim().length() == 0){
                this.aviso("O campo Cobrador não foi informado");
                return false;
        }   else if(umControleMotorista.pesquisarMotorista(jTextFieldMotoristaOnibus.getText()) == null){
                this.aviso("O motorista não está registrado");
                return false;
                
        }   else if(umControleCobrador.pesquisarCobrador(jTextFieldCobradorOnibus.getText()) == null){
                this.aviso("O cobrador não está registrado");
                return false;
                
        }
        else return true;
    }
    
    
    public static void main(String args[]) {
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ControleRodoviario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ControleRodoviario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ControleRodoviario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ControleRodoviario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new ControleRodoviario().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelarOnibus;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonExcluirOnibus;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonSalvarFuncionario;
    private javax.swing.JButton jButtonSalvarOnibus;
    private javax.swing.JComboBox jComboBoxTipo;
    private javax.swing.JLabel jLabelCobradorOnibus;
    private javax.swing.JLabel jLabelData;
    private javax.swing.JLabel jLabelHorarioSaida;
    private javax.swing.JLabel jLabelMotoristaOnibus;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNomeDaLinha;
    private javax.swing.JLabel jLabelNumeroDaLinha;
    private javax.swing.JLabel jLabelNumeroIdenficacao;
    private javax.swing.JLabel jLabelPlaca;
    private javax.swing.JLabel jLabelTipo;
    private javax.swing.JLabel jLabelTituloFuncionario;
    private javax.swing.JLabel jLabelTituloOnibus;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableListaFuncionarios;
    private javax.swing.JTable jTableListaOnibus;
    private javax.swing.JTextField jTextFieldCobradorOnibus;
    private javax.swing.JTextField jTextFieldData;
    private javax.swing.JTextField jTextFieldHorarioSaida;
    private javax.swing.JTextField jTextFieldMotoristaOnibus;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeDaLinha;
    private javax.swing.JTextField jTextFieldNumeroDaLinha;
    private javax.swing.JTextField jTextFieldNumeroIdentificacao;
    private javax.swing.JTextField jTextFieldPlaca;
    // End of variables declaration//GEN-END:variables
}
