package teste;

import static org.junit.Assert.*;

import model.Funcionario;

import org.junit.Before;
import org.junit.Test;

import control.ControleFuncionario;

public class ControleFuncionarioTest {
	Funcionario umFuncionario;
	ControleFuncionario umControleFuncionario;
	
	@Before
	public void setUp() throws Exception{
		umFuncionario = new Funcionario("Funcionario", "1111");		
		umControleFuncionario = new ControleFuncionario();
		umControleFuncionario.adicionarFuncionario(umFuncionario);
	}
	
	@Test
	public void testRemoverFuncionario() {
		umControleFuncionario.removerFuncionario(umFuncionario);
		assertNull(umControleFuncionario.pesquisarFuncionario("Funcionario"));
	}

	@Test
	public void testPesquisarFuncionario() {
		assertNotNull(umControleFuncionario.pesquisarFuncionario("Funcionario"));
	}

}
