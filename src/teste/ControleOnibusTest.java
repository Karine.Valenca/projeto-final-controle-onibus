package teste;

import static org.junit.Assert.*;

import model.Onibus;

import org.junit.Before;
import org.junit.Test;

import control.ControleOnibus;

public class ControleOnibusTest {
	Onibus umOnibus;
	ControleOnibus umControleOnibus;

	@Before
	public void setUp() throws Exception{
		umOnibus = new Onibus("placa");
		umControleOnibus = new ControleOnibus();
		umControleOnibus.adicionarOnibus(umOnibus);		
	}
	
	@Test
	public void testRemoverOnibus() {
		umControleOnibus.removerOnibus(umOnibus);
		assertNull(umControleOnibus.pesquisarOnibus("placa"));
	}

	@Test
	public void testPesquisarOnibus() {
		assertNotNull(umControleOnibus.pesquisarOnibus("placa"));
	}

}
