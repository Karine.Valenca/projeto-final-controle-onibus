package teste;

import static org.junit.Assert.*;

import model.Motorista;

import org.junit.Before;
import org.junit.Test;

import control.ControleMotorista;

public class ControleMotoristaTest {
	
	Motorista umMotorista;
	ControleMotorista umControleMotorista;
	
	@Before
	public void setUp() throws Exception{
		umMotorista = new Motorista("Motorista", "1111");
		umControleMotorista = new ControleMotorista();
		umControleMotorista.adicionarMotorista(umMotorista);
	}	

	@Test
	public void testRemoverMotorista() {
		umControleMotorista.removerMotorista(umMotorista);
		assertNull(umControleMotorista.pesquisarMotorista("Motorista"));
		
	}

	@Test
	public void testPesquisarMotorista() {
		assertNotNull(umControleMotorista.pesquisarMotorista("Motorista"));
	}

}
