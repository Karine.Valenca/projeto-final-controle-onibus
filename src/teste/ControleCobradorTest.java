package teste;

import static org.junit.Assert.*;

import model.Cobrador;

import org.junit.Before;
import org.junit.Test;

import control.ControleCobrador;

public class ControleCobradorTest {
	
	Cobrador umCobrador;
	ControleCobrador umControleCobrador;
	
	@Before
	public void setUp() throws Exception{
		umCobrador = new Cobrador("Cobrador", "1111");
		umControleCobrador = new ControleCobrador();
		umControleCobrador.adicionarCobrador(umCobrador);
		
	}

	@Test
	public void testRemoverCobrador() {
		umControleCobrador.removerCobrador(umCobrador);
		assertNull(umControleCobrador.pesquisarCobrador("Cobrador"));
	}

	@Test
	public void testPesquisarCobrador() {		
		assertNotNull(umControleCobrador.pesquisarCobrador("Cobrador"));
	}

}
