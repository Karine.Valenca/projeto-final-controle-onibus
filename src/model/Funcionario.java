package model;

import java.io.Serializable;

public class Funcionario implements Serializable {
	protected String nome;
	protected String numeroIdentificacao;
        public String tipo;
	
	public Funcionario(){
		
	}
	
	public Funcionario(String nome, String numeroIdentificacao){
		this.nome = nome;
                this.numeroIdentificacao = numeroIdentificacao;
	}
	
	//getters e setters
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNumeroIdentificacao() {
		return numeroIdentificacao;
	}
	public void setNumeroIdentificacao(String numeroIdentificacao) {
		this.numeroIdentificacao = numeroIdentificacao;
	}
	
	
}
