package model;

public class Onibus {
	private String nomeDaLinha;
	private String numeroDaLinha;
	private String placa;
        private String data;
	private String horarioSaida;
	private Motorista Motorista;
	private Cobrador Cobrador;
	
	public Onibus(){
		
	}
	
	public Onibus(String nomeDaLinha, String numeroDaLinha, String placa, 
                String data, String horarioSaida){
		this.nomeDaLinha = nomeDaLinha;
                this.numeroDaLinha = numeroDaLinha;
		this.placa = placa;
                this.data = data;
                this.horarioSaida = horarioSaida;
		
	}
	
	//Getters e Setters
	public String getNomeDaLinha() {
		return nomeDaLinha;
	}
	public void setNomeDaLinha(String nomeDaLinha) {
		this.nomeDaLinha = nomeDaLinha;
	}
	public String getNumeroDaLinha() {
		return numeroDaLinha;
	}
	public void setNumeroDaLinha(String numeroDaLinha) {
		this.numeroDaLinha = numeroDaLinha;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public Motorista getMotorista() {
		return Motorista;
	}
	public void setMotorista(Motorista motorista) {
		Motorista = motorista;
	}
	public Cobrador getCobrador() {
		return Cobrador;
	}
	public void setCobrador(Cobrador cobrador) {
		Cobrador = cobrador;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHorarioSaida() {
		return horarioSaida;
	}

	public void setHorarioSaida(String horarioSaida) {
		this.horarioSaida = horarioSaida;
	}
	
}
